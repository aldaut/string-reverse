/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reversemeasure;

import java.util.Scanner;

/**
 *
 * @author almas
 */
public class ReverseMeasure {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        StringBuilder str2 = new StringBuilder(str);
        System.out.println("строка - "+str);
        System.out.println("строка 2 - "+str2.reverse());
        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000; i++) {
            str2.reverse();
        }
        long finish = System.currentTimeMillis();
        long timeConsumedMillis = finish - start;
        System.out.println("1000 итераций - "+timeConsumedMillis);
        start = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            str2.reverse();
        }
        finish = System.currentTimeMillis();
        timeConsumedMillis = finish - start;
        System.out.println("10 000 итераций - "+timeConsumedMillis);
        start = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            str2.reverse();
        }
        finish = System.currentTimeMillis();
        timeConsumedMillis = finish - start;
        System.out.println("100 000 итераций - "+timeConsumedMillis);
    }

}
